import logo from "./assets/images/logo.png";

export const DaleApp = () => {
    return (
        <>
            {/* <img src={logo} alt="dale.sv" /> */}
            <h1>¡Estamos en mantenimiento!</h1>
            <div>
                <p>Estamos trabajando para mejorar tu experiencia. Volveremos pronto con m&aacute;s novedades.</p>
            </div>
        </>
    )
  }