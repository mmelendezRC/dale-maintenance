import React from 'react';
import ReactDOM from 'react-dom/client';
import { DaleApp } from './DaleApp'

import './styles.css';

ReactDOM.createRoot(document.getElementById("root")).render(
    <React.StrictMode>
        <DaleApp />
    </React.StrictMode>
)